/*
scitizen-storage 0.1.0- Manage storage put and get operation for an image file
https://osallou@bitbucket.org/osallou/scitizen-storage.git
Built on 2016-10-18
*/
var pkgcloud = require('pkgcloud');
var pairtree = require('pairtree');
var fs = require('fs');
var mv = require('mv');
var mkdirp = require('mkdirp');

module.exports = {
    /**
    * Configure the storage
    *
    * type: 'pairtree' or 's3'
    * config: object with storage config, dependant of type.
    *         for pairtree: { path: 'path_to_pairtree_storage' }
    *         for s3:
    *          s3 makes use of *pkgcloud* module, multiple providers
    *          are supported.
    *                  { path: 'path_to_cache_storage_dir' ,
    *                    provider: 'openstack',
    *                    username: 'myuserid',
    *                    password: 'password_identifier',
    *                    region:   's3 region',
    *                    authUrl:  's3 auth url',
    *                    container: 'scitizen'
    *                  }
    */
    configure: function(type, config) {
        if(type!=='pairtree' && type!='s3') {
            return { err: 'Wrong type' };
        }
        storage = type;
        storage_config = config;
        if(storage=='s3') {
            rackspace = pkgcloud.storage.createClient({
                provider: config.provider,
                username: config.username,
                region: config.region,
                password: config.password,
                authUrl: config.authUrl
            });
            rackspace.getContainer(config.container, function(err, container) {
                if(err!==null && 'statusCode' in err && err.statusCode==404) {
                    console.log('container does not exists, creating it...');
                    rackspace.createContainer({ name: config.container },
                        function(err, container) {
                            if(err!==null) {
                                console.log(err);
                            }
                    });
                }

            });
         }
         else {
             fs.exists(storage_config.path, function(exists) {
                if(! exists) { mkdirp.sync(storage_config.path); }
             });
         }
        return {};
    },
    /**
    * Get an element from storage from its unique id
    *
    * image_id: element id to retreive
    * callback: function(errno, image) errno=0 means ok
    */
    get: function(image_id, callback) {
        if(storage=='s3') {
            return get_s3(image_id, callback);
        }
        else {
            return get_pairtree(image_id, callback);
        }

    },
    /**
    * Puts a file element in storage
    *
    * image_id: element id to retreive
    * image_path: path to the file to stora
    * metadata: metadata info for the image (object), not managed for pairtree
    * callback: function(errno, image) errno=0 means ok
    */
    put: function(image_id, image_path, metadata, callback) {
        if(storage=='s3') {
            return put_s3(image_id, image_path, metadata, callback);
        }
        else {
            return put_pairtree(image_id, image_path, metadata, callback);
        }


    },
  /**
  * Delete an element in storage
  *
  * image_id: element id to retreive
  * callback: function(errno, null) errno=0 means ok
  */
    delete: function(image_id, callback) {
        if(storage=='s3') {
            return delete_s3(image_id, callback);
        }
        else {
            return delete_pairtree(image_id, callback);
        }

    }
};

// pairtree or s3
var storage = 0;
var storage_config = null;
var rackspace= null;
var LRU = require('lru-cache');
var options = { max: 100,
                length: function (n) { return n.size/1048576; },
                dispose: function (key, n) {
                fs.exists(storage_config.path + '/' + n.name, function(exists) {
                    if(exists) {
                        fs.unlink(storage_config.path + '/' + n.name);
                    }
                });
              },
              maxAge: 1000 * 60 * 60 };
var cache = LRU(options);
var otherCache = LRU(50); // sets just the max size

function get_s3(image_id, callback) {
    var image = cache.get(image_id);
    if(image === null || image === undefined) {
        var image_path = storage_config.path+'/'+image_id;
        var output_file = fs.createWriteStream(image_path);
        rackspace.download({
            container: config.container,
            remote: image_id
        }, function(err, result) {
            if(err!==null) {
                callback(err.errno, image);
            }
            else {
                result.path = image_id;
                cache.set(image_id, result);
                image = cache.get(image_id);
                callback(0, image);
            }
        }).pipe(output_file);
    }
    else {
        callback(0, image);
    }
}


function get_pairtree(image_id, callback) {
  var image = {id: image_id};
  var image_path = storage_config.path + '/' +
                    pairtree.path(image_id) +
                    '/' + image_id;
  fs.exists(image_path, function(exists) {
    if(exists) {
        image.path = pairtree.path(image_id) + '/' + image_id;
        callback(0, image);
    }
    else {
        callback(404, undefined);
    }
  });
}

function put_s3 (image_id, image_file, metadata, callback) {
    var image = {id: image_id, metadata: metadata};
    rackspace.upload({
            container:  config.container,
            remote: image_id,
            local: image_file,
            metadata: metadata
            }, function(err, result) {
                if(err!==null) {
                    callback(err.errno,null);
                }
                else {
                    callback(0, image);
                }
    });
}

function put_pairtree (image_id, image_file, metadata, callback) {
    var image = {id: image_id};
    var ppath = storage_config.path + '/' + pairtree.path(image_id);
    fs.exists(ppath, function(exists) {
        if(! exists) {
          mkdirp.sync(ppath);
        }
        mv(image_file, ppath + '/' + image_id, function(err) {
            image.path =pairtree.path(image_id) + '/' + image_id;
            if(err) {
                callback(500, err);
            }
            else {
                callback(0, image);
            }
        });
    });

}

function delete_s3 (image_id, callback) {
    cache.del(image_id);
    fs.exists(CONFIG.dir + '/' + image_id, function(exists) {
        if(exists) {
            fs.unlink(storage_config.path + '/' + image_id, function(err) {});
        }
    });

    rackspace.removeFile(config.container, image_id, function(err) {
        if(err!==null) {
            console.log('Failed to delete ' + image_id + ' from S3');
            callback(err.errno,null);
        }
        else {
            callback(0, null);
        }
    });
}

function delete_pairtree (image_id, callback) {
    var ppath = storage_config.path + '/' + pairtree.path(image_id);
    try {
    fs.unlink(ppath + '/' + image_id, function(err) {
        if(err) { callback(500, err); }
        else {
            callback(0,null);
        }
    });
    }
    catch(err) {
      callback(500, err);
    }
}
