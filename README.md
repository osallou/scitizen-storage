Scitizen-storage
================

Node.js module to manage scitizen project image storage in s3 or pairtree structure.


storage.configure(type, config)
storage.put(image_id, file_path, metadata, callback)

Type:

 * pairtree
 * s3

pairtree case
=============

For pairtree, put a file will move the file, not copy it...

    config { path: 'location_of_pairtree_dir'}


s3 case
=======

s3 makes use of *pkgcloud* module, multiple providers
are supported.

    config { path: 'path_to_cache_storage_dir' ,
             provider: 'openstack',
             username: 'myuserid',
             password: 'password_identifier',
             region:   's3 region',
             authUrl:  's3 auth url',
             container: 'scitizen'

