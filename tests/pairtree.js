var os = require('os');
var fs = require('fs');

var expect = require('chai').expect,
    pairtree = require('pairtree'),
    storage = require('../src/index');

var uuid = require('node-uuid');

var pairtree_path = os.tmpdir();


storage.configure('pairtree',  { path: pairtree_path });

describe('put_get_delete', function () {

    it('should copy file to pairtree directory', function () {
      var file_id = uuid.v4();
      var test_file = os.tmpdir()+'/test.txt';
      fs.writeFile(test_file, "Hey there!\n", function(err) {
        expect(err).to.equal(null);
        storage.put(file_id, test_file, {}, function (err, image) {
          expect(err).to.equal(0);
          var expected_file_path = pairtree.path(file_id) + '/' + file_id;
          expect(image.path).to.equal(expected_file_path);
          fs.exists(pairtree_path + '/' + expected_file_path, function(exists) {
            expect(exists).to.equal(true);
          });
        });
      });
    });

    it('get file path from pairtree directory', function () {
      var file_id = uuid.v4();
      var test_file = os.tmpdir() + '/test.txt';
      fs.writeFile(test_file, "Hey there!\n", function(err) {
        expect(err).to.equal(null);
        storage.put(file_id, test_file, {}, function (err, image) {
          storage.get(image.id, function(err, result_image) {
            expect(err).to.equal(0);
            expect(image.path).to.equal(result_image.path);
          });
        });
      });
    });

  it('delete file from pairtree directory', function () {
    var file_id = uuid.v4();
    var test_file = os.tmpdir() + '/test.txt';
    fs.writeFile(test_file, "Hey there!\n", function(err) {
      expect(err).to.equal(null);
      storage.put(file_id, test_file, {}, function (err, image) {
        storage.delete(image.id, function(err, result_image) {
          expect(err).to.equal(0);
          fs.exists(image.path, function(exists) {
            expect(exists).to.equal(false);
          });
        });
      });
    });
  });
});
